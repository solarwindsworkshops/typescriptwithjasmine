
module.exports = {
    entry: "./src/app.ts",
    output: { filename: "./src/appAll.js" },
    module: {
        loaders: [
            {
                test: /.ts$/,
                loader: "ts-loader"
            }
        ]
    },
    resolve: {
        extensions: [".ts", ".js"]
    }
}