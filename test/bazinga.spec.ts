import { Bazinga } from "../src/bazinga"

describe("Bazinga", () => {

    beforeEach(() => {
        this.bazinga = new Bazinga();
    })

    it("do something", () => {
        var result = this.bazinga.doIt();

        expect(result).toBe(28);
    })

    it("yet another test", () => {

        spyOn(this.bazinga, "doIt").and.returnValue(11);

        var resutl = this.bazinga.doIt();

        expect(resutl).toBe(11);
        expect(this.bazinga.doIt).toHaveBeenCalledTimes(1);
    })

    it("mocking interface", () => {
        var someObject = jasmine.createSpyObj('someObject', ['method1', 'method2']);
        someObject.method1.and.returnValue(2);

        expect(someObject.method1()).toBe(2);
    })
})