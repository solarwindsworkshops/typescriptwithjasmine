export interface IBazinga {
    someProperty: number;
    doIt(): number;
}

export class Bazinga implements IBazinga{
    someProperty: number;

    constructor() {
        this.someProperty = 46;
    }

    doIt(): number {
        return 28;
    }
}